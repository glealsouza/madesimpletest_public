<?php

/*--------------------------------------------------------------
|   Teste Made Simple                                           |
|                                                               |
|   Artist Add PHP and html page                                |
|                                                               |
|   Desenv.: Guilherme Leal                                     |
|                                                               |
|   Atualização : 02/08/2019                                    |
|                                                               |
|_______________________________________________________________|
*/


include  'variaveis.php';
global $caminhop;
require_once($_SERVER['DOCUMENT_ROOT'].$caminhop.'/controller/validate.php');

if(isset($_POST['user_name'])){
    $username = htmlspecialchars($_POST['user_name']);
}

if(isset($_POST['pwd'])){
    $pwd = htmlspecialchars($_POST['pwd']);
}

if(isset($username) and isset($pwd)){
    valida_acesso($username, $pwd);
}else{
    valida_acesso(null, null);
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Music Collection app - Artist Add</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" type="image/png" href="imgs/icons/ico150x150.png"/>
        
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" type="text/css" href="css/ncv_style.css?36">

</head>
<body  >
    
    <section class = "mainpage" id="mainpage"  >  
        <div class="container">
            <div class="wrap-mainpage">
                
                <center><h2>Artist Add</h2></center>
                <br/>
                <br/>
                <form id = "frmartistadd" name = "frmartistadd" 
                    class="form-horizontal" 
                    role="form"  
                    action="controller/RestController.php" 
                    method="post">
                            
                    <div class="form-group">
                        <label for="name">Artist Name</label>
                        <input tabindex = "0" type="text" class="form-control" id="artist_name" name = "artist_name" required placeholder="Please type Artist Name">
                    </div>
                    
                    <div class="form-group">
                        <label for="pwd">Twitter Handle</label>
                        <input tabindex = "0" type="text" class="form-control" id="twitter_handle" name="twitter_handle" required placeholder="Twitter Handle">
                    </div>
                   
                    <input type="hidden" id="appform" name="appform" value="artistadd">
                    <input type="hidden" id="appfunc" name="appfunc" value="0">
                    
                    <input type="hidden" id="appuser_name" name="appuser_name" value="<?php echo $username ?>">
                    <input type="hidden" id="apppwd" name="apppwd" value="<?php echo $pwd ?>">
                    
                    
                    <div class="form-group">
                        <button tabindex = "0"  form="frmartistadd"  class="btn btn-default">Add</button>
                    </div>
                </form>
                
                <center>
                
               
               
                
                <button class="btn " onclick="callpage('mainpage.php','<?php echo $username; ?>', '<?php echo $pwd; ?>');"><i class="fa fa-home"></i><br/>Main Page</button>
                <button class="btn " onclick="window.location.href ='index.php'"><i class="fa fa-sign-out"></i><br/>Logout</button>
                </center>
                
            </div>
        </div>
    </section>

    <script src="js/jquery/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/functions.js"></script>
    

</body>
</html>
