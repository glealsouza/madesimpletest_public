<?php
    
/*--------------------------------------------------------------
|   Teste Made Simple                                           |
|                                                               |
|   Dao Album Class                                             |
|                                                               |
|   Target : Bussiness rules using SQL language to Convert      |
|            data to Album Object.                              |
|                                                               |
|   Desenv.: Guilherme Leal                                     |
|                                                               |
|   Atualização : 02/08/2019                                    |
|                                                               |
|_______________________________________________________________|
*/   
    date_default_timezone_set("Brazil/East");
   
    global $caminhop;
    require_once 'servidor.php';
    require_once($_SERVER['DOCUMENT_ROOT'].$caminhop.'/pojo/ALBUM.php');
    require_once($_SERVER['DOCUMENT_ROOT'].$caminhop.'/conn/conexao.php');
    

    Class daoalbum{
            
            public function insert($con, $album){
                global $banco_ms;
                
                $fecharConexao = false;

                if ($con == null) {
                    $fecharConexao = true;
                    $auxcon = new Conexao();
                    $con = $auxcon->Conectar();
                }

            try {
                
                $sql = "INSERT INTO ".$banco_ms.".albums(album_name, album_year, artist_id) ";
                $sql .= "select * from ( select '".$album->getalbum_name()."' as album_name, "
                                             . "'".$album->getalbum_year()."' as album_year, "
                                             . "".$album->getartist_id()." as artist_id "
                                             . ") as tmp where not exists ("
                                             . "select album_name from ".$banco_ms.".albums where album_name = '"
                                             . $album->getalbum_name()."' and artist_id = ".$album->getartist_id()." )";


                $stmt = $con->prepare($sql);
                $stmt->execute();
                
                $auxalbum = new ALBUM();
                $auxalbum = $this->listar($con, "album_name = '".$album->getalbum_name()."' and artist_id = ".$album->getartist_id());
                                
                if(isset($auxalbum)){
                    $album->setalbum_id($auxalbum->getalbum_id());
                }
                
                return $album;
                
            } catch (Exception $e) {
                $retorno = $e->getMessage();
                $retorno = 'Error in Album insert process! ';
                return $retorno;
            }
        }
        
        public function update($con, $album, $optwhere){
                global $banco_ms;
                
                $fecharConexao = false;

                if ($con == null) {
                    $fecharConexao = true;
                    $auxcon = new Conexao();
                    $con = $auxcon->Conectar();
                }

            try {
                
                if(isset($optwhere)){
                    $sql = "UPDATE ".$banco_ms.".albums SET `album_name` = '".$album->getalbum_name()."', ";
                    $sql.= "album_year = '".$album->getalbum_year()."', artist_id = ".$album->getartist_id()." WHERE ".$optwhere;

                    $stmt = $con->prepare($sql);
                    $stmt->execute();

                    $auxalbum = new ALBUM();
                    $auxalbum = $this->listar($con, "album_name = '".$album->getalbum_name()."' and artist_id = ".$album->getartist_id());

                    if(isset($auxalbum)){
                        $album->setalbum_id($auxalbum->getalbum_id());
                    }

                    return $album;
                }
                
            } catch (Exception $e) {
                $retorno = $e->getMessage();
                $retorno = 'Error in Album insert process! ';
                return $retorno;
            }
        }
        
         

        
        public function listar($con, $optwhere){
        
           global $banco_ms;
           $convert = new convertMySQL();
           
           $reg = new ALBUM();
           $fecharConexao = false;

           if ($con == null) {
               $fecharConexao = true;
               $auxcon = new Conexao();
               $con = $auxcon->Conectar();
           }
           
           try{
               
                if(isset($optwhere)){
                    $sql = "SELECT * FROM ".$banco_ms.".albums where ".$optwhere;
                }else{
                    $sql = "SELECT * FROM ".$banco_ms.".albums ";
                }
                $result = $con->query($sql);

                if ($result->num_rows == 1) {
                    $reg = $convert->convertAlbum($result,1 );
                            
                }
                if ($result->num_rows > 1) {
                    $reg = $convert->convertAlbums($result );
                            
                }
                
           } finally {
               if ($fecharConexao) {
                   $con->close();
                }
               
           }    
            
           return $reg;
        }
        
        
        public function listar_c_artist_name($con, $optwhere){
        
           global $banco_ms;
           $convert = new convertMySQL();
           
           $reg = new ALBUM();
           $fecharConexao = false;

           if ($con == null) {
               $fecharConexao = true;
               $auxcon = new Conexao();
               $con = $auxcon->Conectar();
           }
           
           try{
               
                if(isset($optwhere)){
                   
                    $sql  = "SELECT * FROM ".$banco_ms.".albums ";
                    $sql .= "inner join ".$banco_ms.".artists on ";
                    $sql .= $banco_ms.".albums.artist_id = ".$banco_ms.".artists.artist_id ";
                    $sql .= "where ".$optwhere;
                    
                }else{
                    
                    $sql  = "SELECT * FROM ".$banco_ms.".albums ";
                    $sql .= "inner join ".$banco_ms.".artists on ";
                    $sql .= $banco_ms.".albums.artist_id = ".$banco_ms.".artists.artist_id;";
                }
                $result = $con->query($sql);

                if ($result->num_rows == 1) {
                    $reg = $convert->convertAlbum($result,1 );
                            
                }
                if ($result->num_rows > 1) {
                    $reg = $convert->convertAlbums($result );
                            
                }
                
           } finally {
               if ($fecharConexao) {
                   $con->close();
                }
               
           }    
            
           return $reg;
        }
    }
    
?>