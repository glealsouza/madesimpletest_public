<?php
    
/*--------------------------------------------------------------
|   Teste Made Simple                                           |
|                                                               |
|   Dao Album Class                                             |
|                                                               |
|   Target : Bussiness rules using SQL language to Convert      |
|            data to Artyist Object.                            |
|                                                               |
|   Desenv.: Guilherme Leal                                     |
|                                                               |
|   Atualização : 02/08/2019                                    |
|                                                               |
|_______________________________________________________________|
*/   
    date_default_timezone_set("Brazil/East");
   
    global $caminhop;
    require_once 'servidor.php';
    require_once($_SERVER['DOCUMENT_ROOT'].$caminhop.'/pojo/ARTIST.php');
    require_once($_SERVER['DOCUMENT_ROOT'].$caminhop.'/conn/conexao.php');
    

    Class daoartist{
            
            public function insert($con, $artist){
                global $banco_ms;
                
                $fecharConexao = false;

                if ($con == null) {
                    $fecharConexao = true;
                    $auxcon = new Conexao();
                    $con = $auxcon->Conectar();
                }

            try {
                               
                $sql = "INSERT INTO ".$banco_ms.".artists(artist_name, twitter_handle) ";
                $sql .= "select * from ( select '".$artist->getartist_name()."' as artist_name, "
                                             . "'".$artist->gettwitter_handle()."' as twitter_handle "
                                             . ") as tmp where not exists ("
                                             . "select artist_name from ".$banco_ms.".artists where artist_name = '"
                                             . $artist->getartist_name()."') ";


                $stmt = $con->prepare($sql);
                $stmt->execute();
                
                $auxartist = new ARTIST();
                $auxartist = $this->listar($con, "artist_name = '".$artist->getartist_name()."'");
                                
                if(isset($auxartist)){
                    $artist->setartist_id($auxartist->getartist_id());
                }
                 
                $retorno = $artist;
                return $retorno;
                
            } catch (Exception $e) {
                $retorno = $e->getMessage();
                $retorno = 'Error in Artist insert process! ';
                return $retorno;
            }
        }
        
        public function update($con, $artist, $optwhere){
                global $banco_ms;
                
                $fecharConexao = false;

                if ($con == null) {
                    $fecharConexao = true;
                    $auxcon = new Conexao();
                    $con = $auxcon->Conectar();
                }

            try {
                    if(isset($optwhere)){
                        $sql = "UPDATE ".$banco_ms.".artists SET `artist_name` = '".$artist->getartist_name()."', ";
                        $sql.= "twitter_handle = '".$artist->gettwitter_handle()."' WHERE ".$optwhere;


                    $stmt = $con->prepare($sql);
                    $stmt->execute();

                    $auxartist = new ARTIST();
                    $auxartist = $this->listar($con, "artist_name = '".$artist->getartist_name()."'");

                    if(isset($auxartist)){
                        $artist->setartist_id($auxartist->getartist_id());
                    }

                    $retorno = $artist;
                
                }         
                
                return $retorno;
                
            } catch (Exception $e) {
                $retorno = $e->getMessage();
                $retorno = 'Error in Artist insert process! ';
                return $retorno;
            }
        }
        
        public function listar($con, $optwhere){
        
           global $banco_ms;
           $convert = new convertMySQL();
           
           $reg = new ARTIST();
           $fecharConexao = false;

           if ($con == null) {
               $fecharConexao = true;
               $auxcon = new Conexao();
               $con = $auxcon->Conectar();
           }
           
           try{
               
                if(isset($optwhere)){
                    $sql = "SELECT * FROM ".$banco_ms.".artists where ".$optwhere;
                }else{
                    $sql = "SELECT * FROM ".$banco_ms.".artists ";
                }
                $result = $con->query($sql);

                if ($result->num_rows == 1) {
                    $reg = $convert->convertArtist($result,1 );
                            
                }
                if ($result->num_rows > 1) {
                    $reg = $convert->convertArtists($result );
                            
                }
                
           } finally {
               if ($fecharConexao) {
                   $con->close();
                }
               
           }    
            
           return $reg;
        }
        
    }
    
?>