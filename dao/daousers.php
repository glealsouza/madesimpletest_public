<?php

/*--------------------------------------------------------------
|   Teste Made Simple                                           |
|                                                               |
|   Dao Album Class                                             |
|                                                               |
|   Target : Bussiness rules using SQL language to Convert      |
|            data to Users Object.                              |
|                                                               |
|   Desenv.: Guilherme Leal                                     |
|                                                               |
|   Atualização : 02/08/2019                                    |
|                                                               |
|_______________________________________________________________|
*/
   
    date_default_timezone_set("Brazil/East");
    
    require_once 'servidor.php';
    require_once 'convertMySQL.php';
        
    global $caminhop;
    
    require_once($_SERVER['DOCUMENT_ROOT'].$caminhop.'/pojo/USERS.php');
    require_once($_SERVER['DOCUMENT_ROOT'].$caminhop.'/conn/conexao.php');
    

    Class daousers{
        
        public function listar($con, $optwhere){
        
           global $banco_ms;
           $convert = new convertMySQL();
           $user = null;

           $fecharConexao = false;

           if ($con == null) {
               $fecharConexao = true;
               $auxcon = new Conexao();
               $con = $auxcon->Conectar();
           }
           
           try{
               
                if(isset($optwhere)){
                    $sql = "SELECT * FROM ".$banco_ms.".users where ".$optwhere;
                }else{
                    $sql = "SELECT * FROM ".$banco_ms.".users ";
                }
                $result = $con->query($sql);

                if ($result->num_rows == 1) {
                    $user = $convert->convertUser($result,1 );
                            
                }
                if ($result->num_rows > 1) {
                    $user = $convert->convertUsers($result );
                            
                }
                
           } finally {
               if ($fecharConexao) {
                   $con->close();
               }
               
           }    
            
           return $user;
        }
        
    }
    
?>