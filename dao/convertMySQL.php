<?php

/*--------------------------------------------------------------
|   Teste Made Simple                                           |
|                                                               |
|   Convert MySQL to objects                                    |
|                                                               |
|   Target : Convert row retrieved from MySQL Database to       |
|            Objects User, Artist andAlbum                      |
|                                                               |
|   Desenv.: Guilherme Leal                                     |
|                                                               |
|   Atualização : 02/08/2019                                    |
|                                                               |
|_______________________________________________________________|
*/


    Class convertMySQL{
        
        public function convertUser($rs, $pos) {
            if (mysqli_data_seek($rs, 0)) {
                mysqli_data_seek($rs, $pos);
                $user = new USERS();
                $row = $rs->fetch_assoc();
                $user->setuser_id($row['user_id']);
                $user->setuser_name($row['user_name']);
                $user->setuser_password($row['user_password']);
                
                return $user;
            } else {
                return null;
            }
        }
    
        public function convertUsers($rs) {
           $auser[] = new USERS();

           $qtde = mysqli_num_rows($rs);
           for($i=0;$i<$qtde;$i++){

               $auser[] = $this->convertUser($rs, $i);
           }
           return $auser;

       }
       
       
       public function convertArtist($rs, $pos) {
            if (mysqli_data_seek($rs, 0)) {
                mysqli_data_seek($rs, $pos);
                $artist = new ARTIST();
                $row = $rs->fetch_assoc();
                $artist->setartist_id($row['artist_id']);
                $artist->setartist_name($row['artist_name']);
                $artist->settwitter_handle($row['twitter_handle']);
                
                return $artist;
            } else {
                return null;
            }
        }
       
        public function convertArtists($rs) {
           $aartist[] = new ARTIST();

           $qtde = mysqli_num_rows($rs);
           for($i=0;$i<$qtde;$i++){

               $aartist[$i] = $this->convertArtist($rs, $i);
           }
           return $aartist;

       }
       
       public function convertAlbum($rs, $pos) {
            $auxArtist = new ARTIST();
            if (mysqli_data_seek($rs, 0)) {
                mysqli_data_seek($rs, $pos);
                $album = new ALBUM();
                $row = $rs->fetch_assoc();
                $album->setalbum_id($row['album_id']);
                $album->setartist_id($row['artist_id']);
                $album->setalbum_name($row['album_name']);
                $album->setalbum_year($row['album_year']);
                
                if(isset($row['artist_name'])){
                    $auxArtist->setartist_name($row['artist_name']);
                    $auxArtist->setartist_id($row['artist_id']);
                }
                
                if(isset($row['twitter_handle'])){
                    $auxArtist->settwitter_handle($row['twitter_handle']);
                }
                
                if(isset($auxArtist)){
                    $album->setartist($auxArtist);
                }
                
                return $album;
            } else {
                return null;
            }
        }

        public function convertAlbums($rs) {
           $auser[] = new ALBUM();

           $qtde = mysqli_num_rows($rs);
           for($i=0;$i<$qtde;$i++){

               $aalbum[] = $this->convertAlbum($rs, $i);
           }
           return $aalbum;

        }
       
}

?>