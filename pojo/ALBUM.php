<?php

/*--------------------------------------------------------------
|   Teste Made Simple                                           |
|                                                               |
|   Album Class                                                 |
|                                                               |
    Desenv.: Guilherme Leal                                     |
|                                                               |
|   Atualização : 02/08/2019                                    |
|                                                               |
|_______________________________________________________________|
*/
    global $caminhop;
    require_once($_SERVER['DOCUMENT_ROOT'].$caminhop.'/pojo/ARTIST.php');

    global $caminhop;

    Class ALBUM{
        
        public $album_id = 0;
        public $artist_id = 0;
        public $artist = null;
        public  $album_name = '';
        public $album_year = '';
        
       
        
        function ALBUM() {
            $this->artist = new ARTIST();
        }
        
        public function getalbum_id(){
            return $this->album_id;
        }
        
        public function setalbum_id($album_id){
            $this->album_id = $album_id;
        }
        
         public function getartist(){
            return $this->artist;
        }
        
        public function setartist($artist){
            $this->artist = $artist;
        }
        
        public function getartist_id(){
            return $this->artist_id;
        }
        
        public function setartist_id($artist_id){
            $this->artist_id = $artist_id;
        }
        
        public function getalbum_name(){
            return $this->album_name;
        }
        
        public function setalbum_name($album_name){
            $this->album_name = $album_name;
        }
        
        public function getalbum_year(){
            return $this->album_year;
        }
        
        public function setalbum_year($album_year){
            $this->album_year = $album_year;
        }
        
    }

?>
