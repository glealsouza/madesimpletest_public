<?php

/*--------------------------------------------------------------
|   Teste Made Simple                                           |
|                                                               |
|   Artist Class                                                |
|                                                               |
    Desenv.: Guilherme Leal                                     |
|                                                               |
|   Atualização : 02/08/2019                                    |
|                                                               |
|_______________________________________________________________|
*/


    global $caminhop;

    Class ARTIST{
        
        public $artist_id = 0;
        public  $artist_name = '';
        public $twitter_handle = '';
       
        
        function ARTIST() {
            
        }
        
        public function getartist_id(){
            return $this->artist_id;
        }
        
        public function setartist_id($artist_id){
            $this->artist_id = $artist_id;
        }
        
        public function getartist_name(){
            return $this->artist_name;
        }
        
        public function setartist_name($artist_name){
            $this->artist_name = $artist_name;
        }
        
        public function gettwitter_handle(){
            return $this->twitter_handle;
        }
        
        public function settwitter_handle($twitter_handle){
            $this->twitter_handle = $twitter_handle;
        }
        
    }

?>
