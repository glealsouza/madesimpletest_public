<?php

/*--------------------------------------------------------------
|   Teste Made Simple                                           |
|                                                               |
|   User Class                                                  |
|                                                               |
    Desenv.: Guilherme Leal                                     |
|                                                               |
|   Atualização : 02/08/2019                                    |
|                                                               |
|_______________________________________________________________|
*/


    global $caminhop;

    Class USERS{
        
        public $user_id = 0;
        public $user_name = '';
        public $user_password = '';
       
        
        function USERS() {
            
        }
        
        public function getuser_id(){
            return $this->user_id;
        }
        
        public function setuser_id($user_id){
            $this->user_id = $user_id;
        }
        
        public function getuser_name(){
            return $this->user_name;
        }
        
        public function setuser_name($user_name){
            $this->user_name = $user_name;
        }
        
        public function getuser_password(){
            return $this->user_password;
        }
        
        public function setuser_password($user_password){
            $this->user_password = $user_password;
        }
        
    }

?>
