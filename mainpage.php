<?php

/*--------------------------------------------------------------
|   Teste Made Simple                                           |
|                                                               |
|   MainPage(Menu) PHP and html page                            |
|                                                               |
|   Desenv.: Guilherme Leal                                     |
|                                                               |
|   Atualização : 02/08/2019                                    |
|                                                               |
|_______________________________________________________________|
*/


include  'variaveis.php';
global $caminhop;
require_once($_SERVER['DOCUMENT_ROOT'].$caminhop.'/controller/validate.php');

if(isset($_POST['user_name'])){
    $username = htmlspecialchars($_POST['user_name']);
}

if(isset($_POST['pwd'])){
    $pwd = htmlspecialchars($_POST['pwd']);
}

if(isset($username) and isset($pwd)){
    valida_acesso($username, $pwd);
}else{
    valida_acesso(null, null);
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Music Collection app - Main Page</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" type="image/png" href="imgs/icons/ico150x150.png"/>
        
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" type="text/css" href="css/ncv_style.css?36">

</head>
<body  >
    
    <section class = "menupage" id="menupage"  >  
        <div class="container">
            <div class="wrap-menupage">
                
                <center><h2>Main Page</h2></center>
                <center>
                    <div class=" row">
                        
                        <button class="btn btn-mainpage" onclick="callpage('artistadd.php','<?php echo $username; ?>', '<?php echo $pwd; ?>');"><i class="fa fa-plus"></i>&nbsp;&nbsp;<i class="fa fa-user"></i><br/>Artist Add</button>
                        
                        <button class="btn btn-mainpage" onclick="callpage('artistedit.php','<?php echo $username; ?>', '<?php echo $pwd; ?>');"><i class="fa fa-edit"></i>&nbsp;&nbsp;<i class="fa fa-user"></i><br/>Artist Edit</button>
                        
                        <button class="btn btn-mainpage" onclick="callpage('artistlist.php','<?php echo $username; ?>', '<?php echo $pwd; ?>');"><i class="fa fa-list"></i>&nbsp;&nbsp;<i class="fa fa-user"></i><br/>Artist List</button>
                        
                    </div>
                    <br/>
                    <div class=" row">
                        <button class="btn btn-mainpage" onclick="callpage('albumadd.php','<?php echo $username; ?>', '<?php echo $pwd; ?>');"><i class="fa fa-plus"></i><i class="fa fa-music"></i><br/>Album Add</button>
                        <button class="btn btn-mainpage" onclick="callpage('albumedit.php','<?php echo $username; ?>', '<?php echo $pwd; ?>');"><i class="fa fa-edit"></i><i class="fa fa-music"></i><br/>Album Edit</button>
                        <button class="btn btn-mainpage" onclick="callpage('albumlist.php','<?php echo $username; ?>', '<?php echo $pwd; ?>');"><i class="fa fa-list"></i>&nbsp;<i class="fa fa-music"></i><br/>Album List</button>
                    </div>
                    <div class=" row">
                        <button class="btn btn-mainpage-logout" onclick="window.location.href ='index.php'"><i class="fa fa-sign-out"></i><br/>Logout</button>
                    </div>
                </center>
                
            </div>
        </div>
    </section>

    <script src="js/jquery/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/functions.js"></script>

</body>
</html>