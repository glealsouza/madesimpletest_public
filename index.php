<?php
/*--------------------------------------------------------------
|   Teste Made Simple                                           |
|                                                               |
|   Login PHP and html page                                     |
|                                                               |
|   Desenv.: Guilherme Leal                                     |
|                                                               |
|   Atualização : 02/08/2019                                    |
|                                                               |
|_______________________________________________________________|
*/

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Music Collection app</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" type="image/png" href="imgs/icons/ico150x150.png"/>
        
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" type="text/css" href="css/ncv_style.css?36">

</head>
<body  >
    
    <section class = "login" id="login"  >  
        
        <div class="container">
            
            <div class="wrap-login">
                <center><img  src="imgs/music-1085655_640.png" width="180" ></center>
                <center><h2>Music Collection App</h2></center>
                
                <form id = "frmlogin" name = "frmficha" 
                    class="form-horizontal" 
                    role="form"  
                    action="controller/RestController.php" 
                    method="post">
                            
                    <div class="form-group">
                        <label for="name">Username</label>
                        <input tabindex = "0" type="text" class="form-control" id="name" name = "name" required placeholder="Please type User Name">
                    </div>
                    
                    <div class="form-group">
                        <label for="pwd">Password.</label>
                        <input tabindex = "0" type="password" class="form-control" id="pwd" name="pwd" required placeholder="personal password">
                    </div>
                   
                    <input type="hidden" id="appform" name="appform" value="login">
                    <input type="hidden" id="appfunc" name="appfunc" value="0">
                    
                    
                    <div class="form-group">
                        <button tabindex = "0"  form="frmlogin"  class="btn btn-default">Sign In</button>
                    </div>
                </form>
    
            </div>
                
        </div>
        
    </section>

    <script src="js/jquery/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    

</body>
</html>