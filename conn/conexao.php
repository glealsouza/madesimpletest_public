<?php

/*--------------------------------------------------------------
|   Teste Made Simple                                           |
|                                                               |
|   Connection Class                                            |
|                                                               |
|   Target : MySQL Database Access                              |
|   Desenv.: Guilherme Leal                                     |
|                                                               |
|   Atualização : 02/08/2019                                    |
|                                                               |
|_______________________________________________________________|
*/
global $caminhop;    
require($_SERVER['DOCUMENT_ROOT'].$caminhop.'/dao/servidor.php');

    
    
    Class conexao extends Exception{
        
        private $conn = null;
        
        public function conectar() {
            
            global $serv_ms;
            global $user_ms;
            global $pwd_ms;
            global $banco_ms;        
            
            $this->conn = new mysqli($serv_ms, $user_ms, $pwd_ms, $banco_ms);
            
            // Check connection
            if ($this->conn->connect_error) {
                die("Connection failed: " . $this->conn->connect_error);
            }
            
            return $this->conn;
        } 
        
        public function fechar(){
            mysqli_close($this->conn);
        }
            
    }
    
//    $teste = new conexao();
//    $teste->conectar();
   
?>
