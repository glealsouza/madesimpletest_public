<?php

/*--------------------------------------------------------------
|   Teste Made Simple                                           |
|                                                               |
|   Artist Edit PHP and html page                               |
|                                                               |
|   Desenv.: Guilherme Leal                                     |
|                                                               |
|   Atualização : 02/08/2019                                    |
|                                                               |
|_______________________________________________________________|
*/


include  'variaveis.php';
global $caminhop;

require_once($_SERVER['DOCUMENT_ROOT'].$caminhop.'/controller/validate.php');
require_once($_SERVER['DOCUMENT_ROOT'].$caminhop.'/controller/music_collection_handler.php');

if(isset($_POST['user_name'])){
    $username = htmlspecialchars($_POST['user_name']);
}

if(isset($_POST['pwd'])){
    $pwd = htmlspecialchars($_POST['pwd']);
}

if(isset($username) and isset($pwd)){
    valida_acesso($username, $pwd);
}else{
    valida_acesso(null, null);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Music Collection app - Artist Edit</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" type="image/png" href="imgs/icons/ico150x150.png"/>
        
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" type="text/css" href="css/ncv_style.css?36">

</head>
<body  >
    
    <section class = "mainpage" id="mainpage"  >  
        <div class="container">
            <div class="wrap-mainpage">
                
                <center><h2>Artist Edit</h2></center>
                <br/>
                <center>
                <div class ="row">
                        
                    <div class="col-xs-6" style = "margin-top: -10px">
                        <label style = "color: black">Select Artist</label>
                        <select tabindex = "0" class="form-control" id ="artist_sel" style="width:170px;">
                            <?php
                                $mch = new music_collection_Handler();
                                $alist = $mch->artists_list(null);
                                $indice = 0;
                                
                                foreach ($alist as $auxart) {
                                    echo '<option id = "opsartist'.$indice.'" value="'.$auxart->getartist_id().'">'.$auxart->getartist_name().'</option>';
                                    $indice++;
                                }
                                
                            ?>
                                
                        </select>
                    </div>

                    <div class="col-xs-6" style = "margin-top: 3px;">
                        <label for="name"></label>
                        <button onclick = 'load_artist_profile(<?php echo json_encode((array)$alist); ?>);' tabindex = "0"  class="btn btn-default">Load</button>
                    </div>
                </div>
                </center>
                <br/>
                
                <form id = "frmartistedit" name = "frmartistedit" 
                    class="form-horizontal" 
                    role="form"  
                    action="controller/RestController.php" 
                    method="post">
                        
                    
                    
                    <div class="form-group">
                        <label for="name">Artist Name</label>
                        <input tabindex = "0" type="text" class="form-control" id="artist_name" name = "artist_name" required placeholder="Please type Artist Name">
                    </div>
                    
                    <div class="form-group">
                        <label for="pwd">Twitter Handle</label>
                        <input tabindex = "0" type="text" class="form-control" id="twitter_handle" name="twitter_handle" required placeholder="Twitter Handle">
                    </div>
                   
                    <input type="hidden" id="appform" name="appform" value="artistedit">
                    <input type="hidden" id="appfunc" name="appfunc" value="0">
                    <input type="hidden" id="recid" name="recid" value="">
                    
                    <input type="hidden" id="appuser_name" name="appuser_name" value="<?php echo $username ?>">
                    <input type="hidden" id="apppwd" name="apppwd" value="<?php echo $pwd ?>">
                    
                    
                    <div class="form-group">
                        <button tabindex = "0"  form="frmartistedit"  class="btn btn-default">Update</button>
                    </div>
                </form>
                <center>
               
                   
                    
                <button style = "margin-top: -20px" class="btn " onclick="callpage('mainpage.php','<?php echo $username; ?>', '<?php echo $pwd; ?>');"><i class="fa fa-home"></i><br/>Main Page</button>
                <button style = "margin-top: -20px" class="btn " onclick="window.location.href ='index.php'"><i class="fa fa-sign-out"></i><br/>Logout</button>
                </center>
                
            </div>
        </div>
    </section>

    <script src="js/jquery/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/functions.js"></script>
    

</body>
</html>
