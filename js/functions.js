/*--------------------------------------------------------------
|   Teste Made Simple                                           |
|                                                               |
|   Javascript functions                                        |
|                                                               |
|   Target : to assist pages and php scripts                    |
|   Desenv.: Guilherme Leal                                     |
|                                                               |
|   Atualização : 02/08/2019                                    |
|                                                               |
|_______________________________________________________________|
*/


    function callpage(pageapp, username, pwd){
        var form = document.createElement("form");
        var vname = document.createElement("input");         
        var vpwd = document.createElement("input");         
        
        document.body.appendChild(form);
        form.method = "POST";
        form.action = pageapp;

        vname.name="user_name";
        vname.value = username
        vname.type = 'hidden';
        form.appendChild(vname);

        vpwd.name="pwd";
        vpwd.value = pwd;
        vpwd.type = 'hidden';
        form.appendChild(vpwd);
        
        form.submit();   
        
    }
    
    
    function load_artist_profile(alista){
        
        
        var indice = parseInt(document.getElementById("artist_sel").value);
        var auxlista = JSON.parse(JSON.stringify(alista[indice-1]));
       
        document.getElementById("artist_name").value = auxlista.artist_name;
        document.getElementById("twitter_handle").value = auxlista.twitter_handle;
        document.getElementById("recid").value = auxlista.artist_id;
        
       
    }

    function load_album_profile(alista){
        
        
        var indice = parseInt(document.getElementById("album_sel").value);
        var auxlista = JSON.parse(JSON.stringify(alista[indice-1]));
        
        document.getElementById("album_name").value = auxlista.album_name;
        document.getElementById("album_year").value = auxlista.album_year;
        document.getElementById("recid").value = auxlista.album_id;
        
        document.getElementById('album_artist_sel').value = parseInt(auxlista.artist_id);
        $("#album_artist_sel").val(parseInt(auxlista.artist_id)).trigger("change");
        
        //location.href = "#edicao";
    }