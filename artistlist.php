<?php

/*--------------------------------------------------------------
|   Teste Made Simple                                           |
|                                                               |
|   Artist List PHP and html page                               |
|                                                               |
|   Desenv.: Guilherme Leal                                     |
|                                                               |
|   Atualização : 02/08/2019                                    |
|                                                               |
|_______________________________________________________________|
*/


include  'variaveis.php';
global $caminhop;
require_once($_SERVER['DOCUMENT_ROOT'].$caminhop.'/controller/validate.php');
require_once($_SERVER['DOCUMENT_ROOT'].$caminhop.'/controller/music_collection_handler.php');

if(isset($_POST['user_name'])){
    $username = htmlspecialchars($_POST['user_name']);
}

if(isset($_POST['pwd'])){
    $pwd = htmlspecialchars($_POST['pwd']);
}

if(isset($username) and isset($pwd)){
    valida_acesso($username, $pwd);
}else{
    valida_acesso(null, null);
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Music Collection app - Artist List</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" type="image/png" href="imgs/icons/ico150x150.png"/>
        
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" type="text/css" href="css/ncv_style.css?36">

</head>
<body  >
    
    <section class = "mainpage" id="mainpage"  >  
        <div class="container">
            <div class="wrap-mainpage">
                
                <center><h2>Artist List</h2></center>
                <center>
                <br/>
                
                
                <table id="tbartists" class="table table-striped table-bordered table-sm table-wrapper-scroll-y my-custom-scrollbar" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            
                          <th class="th-sm" style = "color: black; font-stretch: condensed">Artist ID
                          </th>
                          <th class="th-sm" style = "color: black; font-stretch: condensed">Artist Name
                          </th>
                          <th class="th-sm" style = "color: black; font-stretch: condensed">Twitter Handle
                          </th>
                        </tr>
                    </thead>
                    <tbody>
                        
                <?php
                    $mch = new music_collection_Handler();
                    $alist = $mch->artists_list(null);
                    
                    foreach ($alist as $auxart) {
                        echo '<tr>';
                        echo '<td style = "width: 13ch;color: black">'.$auxart->getartist_id().'</td>';
                        echo '<td style = "color: black">'.$auxart->getartist_name().'</td>';
                        echo '<td style = "color: black">'.$auxart->gettwitter_handle().'</td>';
                        echo '</tr>';
                    }
                ?>
                </tbody>
               
              </table>
                
               
                <button class="btn " onclick="callpage('mainpage.php','<?php echo $username; ?>', '<?php echo $pwd; ?>');"><i class="fa fa-home"></i><br/>Main Page</button>
                <button class="btn " onclick="window.location.href ='index.php'"><i class="fa fa-sign-out"></i><br/>Logout</button>
                </center>
                
            </div>
        </div>
    </section>

    <script src="js/jquery/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/functions.js"></script>


</body>
</html>
