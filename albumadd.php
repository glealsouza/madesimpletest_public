<?php

/*--------------------------------------------------------------
|   Teste Made Simple                                           |
|                                                               |
|   Album Add PHP and html page                                 |
|                                                               |
    Desenv.: Guilherme Leal                                     |
|                                                               |
|   Atualização : 02/08/2019                                    |
|                                                               |
|_______________________________________________________________|
*/


include  'variaveis.php';
global $caminhop;
require_once($_SERVER['DOCUMENT_ROOT'].$caminhop.'/controller/validate.php');
require_once($_SERVER['DOCUMENT_ROOT'].$caminhop.'/controller/music_collection_handler.php');

if(isset($_POST['user_name'])){
    $username = htmlspecialchars($_POST['user_name']);
}

if(isset($_POST['pwd'])){
    $pwd = htmlspecialchars($_POST['pwd']);
}
if(isset($username) and isset($pwd)){
    valida_acesso($username, $pwd);
}else{
    valida_acesso(null, null);
}



?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Music Collection app - Album Add</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" type="image/png" href="imgs/icons/ico150x150.png"/>
        
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" type="text/css" href="css/ncv_style.css?36">

</head>
<body  >
    
    <section class = "mainpage" id="mainpage"  >  
        <div class="container">
            <div class="wrap-mainpage">
                
                <center><h2>Album Add</h2></center>
                <br/>
                <br/>
                <form id = "frmalbumadd" name = "frmalbumadd" 
                    class="form-horizontal" 
                    role="form"  
                    action="controller/RestController.php" 
                    method="post">
                            
                    <div class="form-group">
                        <label for="name">Album Name</label>
                        <input tabindex = "0" type="text" class="form-control" id="album_name" name = "album_name" required placeholder="Please type Artist Name">
                    </div>
                    
                    <div class ="row">
                        <div class="form-group col-xs-5">
                            <label style = "margin-top: +15px" for="pwd">Album Year</label>
                        </div>
                        <div class="form-group col-xs-7">
                            <input tabindex = "0" type="text" class="form-control" id="album_year" name="album_year" required placeholder="Year of Publishing">
                        </div>
                    </div>
                    
                    <div class="form-group" >
                        <select tabindex = "0" class="form-control" id ="album_artist_sel" name ="album_artist_sel">
                            <option>Select Artist</option>
                            <?php
                                $mch = new music_collection_Handler();
                                $alist = $mch->artists_list(null);
                                $indice = 0;
                                
                                foreach ($alist as $auxart) {
                                    echo '<option id = "opsartist'.$indice.'" value="'.$auxart->getartist_id().'">'.$auxart->getartist_name().'</option>';
                                    $indice++;
                                }
                                
                            ?>
                                
                        </select>
                    </div>
                    
                    <input type="hidden" id="appform" name="appform" value="albumadd">
                    <input type="hidden" id="appfunc" name="appfunc" value="0">
                    
                    <input type="hidden" id="appuser_name" name="appuser_name" value="<?php echo $username ?>">
                    <input type="hidden" id="apppwd" name="apppwd" value="<?php echo $pwd ?>">
                    
                    <br/>
                    <div class="form-group">
                        <button tabindex = "0"  form="frmalbumadd"  class="btn btn-default">Add</button>
                    </div>
                </form>
                
                <center>
                
                
                <button  style = "margin-top: -30px" class="btn " onclick="callpage('mainpage.php','<?php echo $username; ?>', '<?php echo $pwd; ?>');"><i class="fa fa-home"></i><br/>Main Page</button>
                <button style = "margin-top: -30px" class="btn " onclick="window.location.href ='index.php'"><i class="fa fa-sign-out"></i><br/>Logout</button>
                </center>
                
            </div>
        </div>
    </section>

    <script src="js/jquery/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/functions.js"></script>
    

</body>
</html>
