<?php

/*--------------------------------------------------------------
|   Teste Made Simple                                           |
|                                                               |
|   Music Collection Class                                      |
|                                                               |
|   Target : Add, Update and List functions                     |
|   Desenv.: Guilherme Leal                                     |
|                                                               |
|   Atualização : 02/08/2019                                    |
|                                                               |
|_______________________________________________________________|
*/

    if(isset($caminhop)){
    }else{
        include '../variaveis.php';
        global $caminhop;
    }
    
    
    require_once($_SERVER['DOCUMENT_ROOT'].$caminhop.'/pojo/USERS.php');
    require_once($_SERVER['DOCUMENT_ROOT'].$caminhop.'/dao/daousers.php');
    
    require_once($_SERVER['DOCUMENT_ROOT'].$caminhop.'/pojo/ARTIST.php');
    require_once($_SERVER['DOCUMENT_ROOT'].$caminhop.'/dao/daoartist.php');
    
    require_once($_SERVER['DOCUMENT_ROOT'].$caminhop.'/pojo/ALBUM.php');
    require_once($_SERVER['DOCUMENT_ROOT'].$caminhop.'/dao/daoalbum.php');
    

    class music_collection {
        
        public function signin($recebido){
            $user = new USERS();
            $daousers = new daousers();
        
            $user = $daousers->listar(null, 'user_name = "'.$recebido['name'].'" and user_password ="'.$recebido['pwd'].'"');
            return $user;
           
       }
       
       
       public function artistadd($recebido){
           
           $artist = new ARTIST();
           $daoart = new daoartist();
            
            if(isset($recebido["artist_name"])){
                $artist->setartist_name(htmlspecialchars($_POST['artist_name']));
            }
            
            if(isset($recebido["twitter_handle"])){
                $artist->settwitter_handle(htmlspecialchars($_POST['twitter_handle']));
            }
           
            $saida = $daoart->insert(null,$artist);
         
            return $saida;
            
       }
       
       public function artistUpdate($recebido){
           $artist = new ARTIST();
           $daoart = new daoartist();
            
            if(isset($recebido["artist_name"])){
                $artist->setartist_name(htmlspecialchars($_POST['artist_name']));
            }
            
            if(isset($recebido["twitter_handle"])){
                $artist->settwitter_handle(htmlspecialchars($_POST['twitter_handle']));
            }
            
            if(isset($recebido["recid"])){
                $optwhere = htmlspecialchars($_POST['recid']);
                $optwhere = " artist_id = ".$optwhere;
            }
           
            $saida = $daoart->update(null,$artist, $optwhere);
         
            return $saida;
       }
  
       public function albumadd($recebido){
           
           $album = new ALBUM();
           $daoalb = new daoalbum();
            
            if(isset($recebido["album_name"])){
                $album->setalbum_name(htmlspecialchars($_POST['album_name']));
            }
            
            if(isset($recebido["album_year"])){
                $album->setalbum_year(htmlspecialchars($_POST['album_year']));
            }
            
            if(isset($recebido["album_artist_sel"])){
                $album->setartist_id(htmlspecialchars($_POST['album_artist_sel']));
            }
           
            $saida = $daoalb->insert(null,$album);
         
            return $saida;
            
       }
       
       public function albumUpdate($recebido){
           $album = new ALBUM();
           $daoalb = new daoalbum();
            
            if(isset($recebido["album_name"])){
                $album->setalbum_name(htmlspecialchars($_POST['album_name']));
            }
            
            if(isset($recebido["album_year"])){
                $album->setalbum_year(htmlspecialchars($_POST['album_year']));
            }
            
            if(isset($recebido["album_artist_sel"])){
                $album->setartist_id(htmlspecialchars($_POST['album_artist_sel']));
            }
            
            if(isset($recebido["recid"])){
                $optwhere = htmlspecialchars($_POST['recid']);
                $optwhere = " album_id = ".$optwhere;
            }
           
            $saida = $daoalb->update(null,$album, $optwhere);
         
            return $saida;
       }
  
       
       
       
       
       
       public function artistlist($optwhere){
           $artist = new ARTIST();
           $daoart = new daoartist();
           $saida = $daoart->listar(null, $optwhere);
           return $saida;
       }
       
       public function albumlist($optwhere){
           $alblist = new ALBUM();
           $daoalb = new daoalbum();
           $saida = $daoalb->listar(null, $optwhere);
           return $saida;
       }
       
       public function albumlist_c_artist_name($optwhere){
           $alblist = new ALBUM();
           $daoalb = new daoalbum();
           $saida = $daoalb->listar_c_artist_name(null, $optwhere);
           return $saida;
       }
       
       
    }

?>