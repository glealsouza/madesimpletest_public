<?php
/*--------------------------------------------------------------
|   Teste Made Simple                                           |
|                                                               |
|   function valida_acesso                                      |
|                                                               |
|   Target : User validation rule, after Login process          |
|   Info   : Just for this test, the function only check if     |
|            global variables, username and pwd are filled.     |
|            This prevent the PHP pages running without         |
|            validation.                                        | 
|                                                               |
|   Info 2 : Login process validate username and password       |
|            comparing the values typed and Database data.      |
|   Desenv.: Guilherme Leal                                     |
|                                                               |
|   Atualização : 02/08/2019                                    |
|                                                               |
|_______________________________________________________________|
*/
        
    function valida_acesso($serv, $pag){
        $result = false;
        if(isset($serv) and isset($pag)){
            if(strlen($serv) >= 3 or strlen($pag)>=3){
                $result = true;
            }
        }
        
        if(!$result){
            $loginURL="index.php";
            echo ("<script>location.href='$loginURL';</script>");
        }
    }
?>
