<?php

/*--------------------------------------------------------------
|   Teste Made Simple                                           |
|                                                               |
|   Connection Class                                            |
|                                                               |
|   Target : Middle                                             |
|   Desenv.: Guilherme Leal                                     |
|                                                               |
|   Atualização : 02/08/2019                                    |
|                                                               |
|_______________________________________________________________|
*/

require_once('music_collection_handler.php');

include  '../variaveis.php';
global $caminhop;

$user_name = '';	
$pwd ='';
        
if(isset($_POST["appform"])){

    $formulario = $_POST["appform"];
    $funcao = $_POST["appfunc"];
    
    if(isset($_POST['name'])){
        $user_name = htmlspecialchars($_POST['name']);
    }

    if(isset($_POST['pwd'])){
        $pwd = htmlspecialchars($_POST['pwd']);
    }
    
    ?>
    <!DOCTYPE html>
    <html lang="en">

    <html>
        <body>
            <script src="../js/functions.js"></script>
       
    <?php    
    switch($formulario){

        case "login":

            if($funcao=="0"){

                $login = new music_collection_Handler();

                try{
                    $result = $login->signin($_POST);
                    if(isset($result)){
                        ?>
                        
                            <script type='text/javascript'>
                                callpage("../mainpage.php", "<?php echo $result->getuser_name() ?>","<?php echo $result->getuser_password() ?>");
                            </script>
                                
                        <?php
                        
                    }else{
                        $loginURL="../index.php";
                        echo ("<script>window.location.href ='$loginURL';alert('Sorry, we couldn\'t find an account with this username. Please check you\'re using the right username and try again.');</script>");
                    }

                } catch (Exception $ex) {
                    $loginURL="../index.php";
                    echo ("<script>window.location.href ='$loginURL';alert('Sorry, An error occurred during login. Please try again or contact the administrator.');</script>");
                    
                }
              
            }

        break;
        
        case "artistadd":

            if($funcao=="0"){
                $artisthandle = new music_collection_Handler();

                try{
                    $result = $artisthandle->artistadd($_POST);
                    if(isset($result)){
                        ?>
                        
                            <script type='text/javascript'>
                                alert('the Artist was successfully included!');
                                callpage("../artistadd.php", "<?php echo $user_name ?>","<?php echo $pwd ?>");
                            </script>
                                                          
                        <?php
                        
                    }else{
                        $loginURL="../index.php";
                        echo ("<script>window.location.href ='$loginURL';alert('Sorry, we couldn\'t add the Artist Profile!');</script>");
                    }

                } catch (Exception $ex) {
                    $loginURL="../index.php";
                    echo ("<script>window.location.href ='$loginURL';alert('Sorry, An error occurred during Artist Add Process. Please try again or contact the administrator.');</script>");
                    
                }
            }
            break;
            
            case "artistedit":

                if($funcao=="0"){
                    $artisthandle = new music_collection_Handler();
                    
                    try{
                        $result = $artisthandle->artistupdate($_POST);
                        if(isset($result)){
                            ?>
                            
                                <script type='text/javascript'>
                                    alert('the Artist Profile was successfully updated!');
                                    callpage("../artistedit.php", "<?php echo $user_name ?>","<?php echo $pwd ?>");
                                </script>
                                
                            <?php

                        }else{
                            $loginURL="../index.php";
                            echo ("<script>window.location.href ='$loginURL';alert('Sorry, we couldn\'t Update the Artist Profile!');</script>");
                        }

                    } catch (Exception $ex) {
                        $loginURL="../index.php";
                        echo ("<script>window.location.href ='$loginURL';alert('Sorry, An error occurred during Artist Edit Process. Please try again or contact the administrator.');</script>");

                    }
                };
                
            break;
                
            case "albumadd":

            if($funcao=="0"){
                $albumthandle = new music_collection_Handler();

                try{
                    $result = $albumthandle->albumadd($_POST);
                    if(isset($result)){
                        ?>
                        
                            <script type='text/javascript'>
                                alert('the Album was successfully included!');
                                callpage("../albumadd.php", "<?php echo $user_name ?>","<?php echo $pwd ?>");
                            </script>
                                                          
                        <?php
                        
                    }else{
                        $loginURL="../index.php";
                        echo ("<script>window.location.href ='$loginURL';alert('Sorry, we couldn\'t add the Album!');</script>");
                    }

                } catch (Exception $ex) {
                    $loginURL="../index.php";
                    echo ("<script>window.location.href ='$loginURL';alert('Sorry, An error occurred during Album Add Process. Please try again or contact the administrator.');</script>");
                    
                }
            }
            break;
            
            case "albumedit":

            if($funcao=="0"){
                $albumthandle = new music_collection_Handler();

                try{
                    $result = $albumthandle->albumupdate($_POST);
                    if(isset($result)){
                        ?>
                        
                            <script type='text/javascript'>
                                alert('the Album was successfully updated!');
                                callpage("../albumedit.php", "<?php echo $user_name ?>","<?php echo $pwd ?>");
                            </script>
                                                          
                        <?php
                        
                    }else{
                        $loginURL="../index.php";
                        echo ("<script>window.location.href ='$loginURL';alert('Sorry, we couldn\'t update the Album!');</script>");
                    }

                } catch (Exception $ex) {
                    $loginURL="../index.php";
                    echo ("<script>window.location.href ='$loginURL';alert('Sorry, An error occurred during Album Update Process. Please try again or contact the administrator.');</script>");
                    
                }
            }
            break;
            
    }


}

?>
    
    </body>
</html>                                   
                                