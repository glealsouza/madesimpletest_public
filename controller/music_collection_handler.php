<?php

/*--------------------------------------------------------------
|   Teste Made Simple                                           |
|                                                               |
|   Music Collection Handle                                     |
|                                                               |
|   Target : Conecton between music collection class and        |
|            RestController Class                               |
|                                                               |
|   Desenv.: Guilherme Leal                                     |
|                                                               |
|   Atualização : 02/08/2019                                    |
|                                                               |
|_______________________________________________________________|
*/

require_once("music_collection.php");
global $nomeusuario;
		
class music_collection_Handler {


	public function signin($recebido) {	

		$lsignin = new music_collection();
		$user = $lsignin->signin($recebido);
                if(isset($user)){
                    $nomeusuario = $user->getuser_name();
                }
                return $user;
	}
	
        public function artistadd($recebido) {	

		$artadd = new music_collection();
		$artist = $artadd->artistadd($recebido);
                
                return $artist;
	}
        
        public function artistupdate($recebido) {	

		$artadd = new music_collection();
		$artist = $artadd->artistUpdate($recebido);
                
                return $artist;
	}
        
        public function artists_list($optwhere){
            $artlist = new music_collection();
            $result = $artlist->artistlist($optwhere);
            return $result;
        }
                
        public function albumadd($recebido) {	

		$albadd = new music_collection();
		$album = $albadd->albumadd($recebido);
                
                return $album;
	}
        
        public function albumupdate($recebido) {	

		$albadd = new music_collection();
		$album = $albadd->albumUpdate($recebido);
                
                return $album;
	}
        
        public function albums_list($optwhere){
            $alblist = new music_collection();
            $result = $alblist->albumlist($optwhere);
            return $result;
        }
        
        public function albums_list_c_artist_name($optwhere){
            $alblist = new music_collection();
            $result = $alblist->albumlist_c_artist_name($optwhere);
            return $result;
        }
        
}


?>